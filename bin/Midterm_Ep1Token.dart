import 'dart:io';

import 'package:test/expect.dart';

void main() {
  List<String> listdata = [];
  String? val = stdin.readLineSync()!;

  var token = (cutGroupToken((makeToken(listdata, val))));
  //print(infixToPosfix(token));
  prints(evaluatePosflix(infixToPosfix(token)));
}

List<String> makeToken(List<String> listdata, var data) {
  var splitdata = data.split(" ");
  for (int i = 0; i < splitdata.length; i++) {
    if (splitdata[i] != "") {
      listdata.add(splitdata[i]);
    } else {
      continue;
    }
  }
  return listdata;
}

List<String> cutGroupToken(List<String> listData) {
  List<String> symbol = ['(', ')', '+', '-', '*', '/'];
  List<String> newList = [];
  String data = '';
  int i;
  for (i = 0; i < listData.length; i++) {
    //newList.add(data);
    if (symbol.contains(listData[i])) {
      newList.add(data);
      newList.add(listData[i]);
      data = "";
    } else {
      data += listData[i];
    }
  }
  newList.add(data);
  return newList;
}

List<String> infixToPosfix(List<String> listData) {
  var value_first = 0, value_last = 5;
  List<String> Symbol = ['(', ')', '+', '-', '*', '/'];
  List<String> operator = [];
  List<String> posflix = [];
  for (int i = 0; i < listData.length; i++) {
    if (!Symbol.contains(listData)) {
      posflix.add(listData[i]);
    }
    if (Symbol.contains(listData)) {
      /*value_first = posflix[i];
      if (operator.isNotEmpty) {
        value_last = operator.last;
      } */
      while (operator.isNotEmpty &&
          operator.last != "(" &&
          value_first <= value_last) {
        posflix.add(operator.removeLast());
      }
      operator.add(posflix[i]);
    }
    if (posflix[i] == '(') {
      operator.add(posflix[i]);
    }
    if (posflix[i] == ')') {
      while (operator.last != "(") {
        posflix.add(operator.removeLast());
      }
      operator.remove("(");
    }
  }
  while (operator.isNotEmpty) {
    posflix.add(operator.removeLast());
  }
  return posflix;
}

double evaluatePosflix(List<String> posflix) {
  List<String> symbol = ['(', ')', '+', '-', '*', '/'];
  List<double> value = [];
  double left, right, sum = 0;

  for (int i = 0; i < posflix.length; i++) {
    if (posflix[i] == "0" ||
        posflix[i] == "0" ||
        posflix[i] == "1" ||
        posflix[i] == "2" ||
        posflix[i] == "3" ||
        posflix[i] == "4" ||
        posflix[i] == "5" ||
        posflix[i] == "6" ||
        posflix[i] == "7" ||
        posflix[i] == "8" ||
        posflix[i] == "9") {
      var a = double.parse(posflix[i]);
      value.add(a);
    } else {
      right = value.removeLast();
      left = value.removeLast();
      if (posflix[i] == "+") {
        sum = left + right;
      }
      if (posflix[i] == "-") {
        sum = left - right;
      }
      if (posflix[i] == "*") {
        sum = left * right;
      }
      if (posflix[i] == "/") {
        sum = left / right;
      }
    }
  }

  return sum;
}
